var gulp = require('gulp'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	rename = require('gulp-rename'),
	watch = require('gulp-watch'),
	livereload= require('gulp-livereload'),
	plumber = require('gulp-plumber'),
	sass = require('gulp-sass'),
	connect = require('gulp-connect'),
	notify = require('gulp-notify'),
	growl = require('gulp-notify-growl');

gulp.task('connect', function() {
	connect.server({
		port: 8000,
		livereload: true
	});
});

var directory = 'assets/';

// Initialize the notifier
var growlNotifier = growl({
  hostname : '10.16.20.25' // IP or Hostname to notify, default to localhost
});

var onError = function (err) {  
	growlNotifier({
		title: 'Error',
		message: err
	});
};

// Concatenate & Minify JS 
gulp.task('scripts', function() {
	return gulp.src(directory+'*.js')
		.pipe(watch())
		.pipe(plumber())
		.pipe(concat('main.js'))
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(uglify())
		.pipe(gulp.dest(directory))
		.pipe(growlNotifier({
			title: 'Success',
			message: 'Compiled Scripts'
		}))
		.pipe(connect.reload());
});

gulp.task('sass', function() {
	return gulp.src(directory+'*.scss')
		.pipe(watch({glob: directory+'*.scss' }))
		.pipe(plumber({
			errorHandler: onError
		}))
		.pipe(sass({
			//errLogToConsole: true,
			outputStyle: 'compressed'
		}))
		.pipe(gulp.dest(directory))
		.pipe(growlNotifier({
			title: 'Success',
			message: 'Compiled Sass'
		}))
		.pipe(connect.reload());
});

// Watch HTML
gulp.task('html', function() {
  return gulp.src('*.html')
	.pipe(watch())
	.pipe(connect.reload());
});

// Default Task
gulp.task('default', ['connect','scripts','sass','html']);
